INVENTOR LG benchmark
================================================================================
This repository is part of a benchmarking activity in the projects:
-  EU Project INVENTOR: INnoVative dEsign of iNstalled airframe componenTs for aircraft nOise Reduction
   - Grant agreement ID: 860538
   - DOI10.3030/860538

Structure
================================================================================
- [README_inventor_benchmark_numericalMethods_definitions.md](README_inventor_benchmark_numericalMethods_definitions.md)
  - Definitions of coordinate system, probing locations, unit conventions, ..
- [script_inventor_benchmark_convertOutput_LAYOUT.py](script_inventor_benchmark_convertOutput_LAYOUT.py)
  - layout script to convert user output data to a unified comparable data format
- [plot_inventor_benchmark.py](plot_inventor_benchmark.py)
  - Python script to plot converted output files using the matplotlib libary
