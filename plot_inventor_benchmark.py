#!/usr/bin/env python3

#-------------------------------------------------------------------------------
#---Description-----------------------------------------------------------------
#-------------------------------------------------------------------------------
# This script generates plots of the results from different partners of the
# INVENTOR project. As input a unified format is expected which is described in
# the file 'README_inventor_benchmark_numericalMethods_definitions.md' and can
# easily be achieved by modifying and using the script layout
# 'script_inventor_benchmark_convertOutput_LAYOUT.py'.
#
#---
#---Usage / Getting started
#---
# 0)  You need any python 3.x version and the packages listed in the next section
# 1)  Adjust the section 'Input' to your need
# 2)  Perform the script to generate the scalar and line plots
#
#---
#---Requirements
#---
# h5py : https://pypi.org/project/h5py
#   pip install h5py
# numpy : https://numpy.org/install/
#   pip install numpy
# matplotlib
#   pip install matplotlib
# scipy
#   pip install scipy
# pandas
#   pip install pandas

#-------------------------------------------------------------------------------
#---Input-----------------------------------------------------------------------
#-------------------------------------------------------------------------------
outputFormat = "pdf"
#outputFormat = "png"
configs         = [
    "BlBkTl",
    "BlBkTlF01",
    "BlBkTlF02",
    "BlBkTlF03",
    "BlBkTlF04",
    "BlBkTlF07",
]
inflowSpeeds    = ["U35"]     # in [m/s] of experiment
dx              = 0.015 # [wheel diameters], resolution of the plotting in dx/D
institutions    = [ # name + directory name where data is located
    ["TUD_exp","TUD_exp/"],
    ["CHALMERS","CHALMERS/"],
    ["ONERA","ONERA/"],
    ["RWTH","RWTH/"],
    ["TUD","TUD/"],
    ["TUD_fullRes","TUD_fullRes/"],
    ]
varNames    = ["uavg", "vavg", "wavg", "urms", "vrms", "wrms"]
varRanges   = {
    "uavg" : [ 0.00, 1.30],
    "vavg" : [-0.30, 0.30],
    "wavg" : [-0.30, 0.30],
    "urms" : [ 0.00, 0.30],
    "vrms" : [ 0.00, 0.30],
    "wrms" : [ 0.00, 0.30],
    "freq._CM[Hz]" :  [200, 12000],
    "SPL_CM[20microPa]" : [0, 60],
    "freq._BF[Hz]" :  [200, 12000],
    "SPL_BF[20microPa]" : [0, 60],
    "freq._13[Hz]" :  [200, 12000],
    "SPL_13[20microPa]" : [0, 60],
    "cp"  : [-1.0,1.0],
    }
# Variable name in file and in plot (LaTex)
varPlotName = {
    "uavg" : "$\overline{u}/u_\infty$",
    "vavg" : "$\overline{v}/u_\infty$",
    "wavg" : "$\overline{w}/u_\infty$",
    "urms" : "RMS($u'$)$/u_\infty$",
    "vrms" : "RMS($v'$)$/u_\infty$",
    "wrms" : "RMS($w'$)$/u_\infty$",
    "x"    : "$x/D$",
    "y"    : "$y/D$",
    "z"    : "$z/D$",
    "freq._CM[Hz]"      : "$f~[Hz]$", # TODO
    "SPL_CM[20microPa]" : "$L_{p/(20 \mu Pa)}~[dB]$", # TODO
    "freq._BF[Hz]"      : "$f~[Hz]$", # TODO
    "SPL_BF[20microPa]" : "$L_{p/(20 \mu Pa)}~[dB]$", # TODO
    "freq._13[Hz]"      : "$f~[Hz]$", # TODO
    "SPL_13[20microPa]" : "$L_{p/(20 \mu Pa), 1/3}~[dB]$", # TODO
    "theta"             : r"$\theta~[^{\circ}]$",
    "cp"                : r"$(p-p_\infty)/(0.5 \rho u_\infty^2)$",
    }
sliceFileNames   = [
    ["planeXp1700", ["z", -0.7, 0.7], ["y", -0.45, 0.5] ],
    ["planeYp0000", ["x", -0.5, 2.0], ["z", -0.7, 0.7] ],
    ["planeYp0500", ["x", -0.5, 2.0], ["z", -0.7, 0.7] ],
    ["planeYp1200", ["x", -0.5, 2.0], ["z", -0.7, 0.7] ],
    #["planeZp0000", ["x", 0.0,0.0], ["y", 0.0, 0.0] ],
    ["planeXn0675", ["z", -0.15, 0.15], ["y", -0.6, 1.25] ],
    ["planeXn0500", ["z", -0.15, 0.15], ["y", -0.6, 1.25] ],
    ["planeXn0300", ["z", -0.15, 0.15], ["y", -0.6, 1.25] ]
    ]

lineFileNames = [
    ["lineYp0000Zp0000", ["x", 0.5,   2.0]  ],
    ["lineXp1700Yp0000", ["z",-0.7,   0.7]  ],
    ["lineXp1700Zp0000", ["y",-0.45,  0.45] ],
    ]

splFileNames = [
    #["microCmFlyv", "freq._CM[Hz]", "SPL_CM[20microPa]" ],
    #["microCmLath", "freq._CM[Hz]", "SPL_CM[20microPa]" ],
    #["microCmFlyv", "freq._BF[Hz]", "SPL_BF[20microPa]" ],
    #["microCmLath", "freq._BF[Hz]", "SPL_BF[20microPa]" ],
    #["microCmFlyv", "freq._13[Hz]", "SPL_13[20microPa]" ],
    #["microCmLath", "freq._13[Hz]", "SPL_13[20microPa]" ],
    ]
cpFileNames = [
    #["cpLeftWheel", ["theta", 0, 360], "cp" ]
    ]

#-------------------------------------------------------------------------------
#---Script----------------------------------------------------------------------
#-------------------------------------------------------------------------------
import h5py
import numpy as np

from os import path

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib.colors as colors
import matplotlib.tri as tri

from scipy.interpolate import griddata
from scipy.spatial import cKDTree

import pandas as pd

# default style defintions
colors = mpl.cycler('color',
    [
      '#000000', # black
      '#006DB2', # blue
      '#BD0006', # red
      '#6BAD00', # green
      '#AD8B00', # oca
      '#AD006B', # magenta
      '#AD4200', # brown
     ])
mpl.rcParams['mathtext.fontset']= 'cm' # latex like
mpl.rcParams['mathtext.rm']     = 'serif'
#mpl.rcParams['figure.constrained_layout.use']  = True
mpl.rcParams['axes.axisbelow']  = True
mpl.rcParams['axes.prop_cycle'] = colors
mpl.rcParams['axes.grid']       = True
mpl.rcParams['grid.color']      = 'gray'  #'k'
mpl.rcParams['grid.linestyle']  = '-'     #':'
mpl.rcParams['grid.linewidth']  = 0.5     #0.5
mpl.rcParams['lines.linewidth'] = 1.0
mpl.rcParams['legend.loc'] = 'lower left'
mpl.rcParams['legend.frameon'] = False

def getFileName(institution, config, inflowSpeed, name):
  return "Inventor_benchmark_{}_{}_{}_{}".format(config,inflowSpeed,name,institution)

#---hdf5 file plot
def plotScalarField(inputFilePath, xVar, yVar, varName, outputFilePath):
  """
    inputFilePath : file path to hdf5 plane
    xVar, yVar    : list containing [coordinateName, rangeMin, rangeMax]
    varName       : variable name to plot
    outputFilePath: where pdf/png is dumped
  """
  print("Wirting {}".format(outputFilePath))
  #---get data
  file  = h5py.File(inputFilePath, 'r')
  xName = xVar[0]
  yName = yVar[0]
  x     = file[xName]
  y     = file[yName]
  if varName not in file:
    print("Var with name {} not found in {}".format(varName,inputFilePath))
    return
  #var   = np.nan_to_num(file[varName], nan=0.0) # zero invalid values
  var = file[varName]
  #---sanity checks
  lenX    = len(x)
  lenY    = len(y)
  lenVar  = len(var)
  if lenX != lenY or lenX != lenVar:
    print("  number of points in {}:{}, {}:{}, {}:{}".format(xName,lenX,yName,lenY,varName,lenVar))
    return
  #---get varRange
  if varName in varRanges:
    varRange = varRanges[varName]
  else:
    varRange = [np.amin(var), np.amax(var)]
  #---plot settings
  fig, ax = plt.subplots()
  ax.set_xlabel(varPlotName[xName])
  ax.set_ylabel(varPlotName[yName])
  noContourLevels = 13  # used number of levels in our color map
  levels=np.linspace(varRange[0],varRange[1],noContourLevels)
  #cmap="RdBu_r" # default paraview red to blue color map
  cmap="jet"    # typical rainbow color map
  useTriangulatedGrid = False # otherwise interpolate to uniform grid
  if useTriangulatedGrid:
    #---triangulate grid
    # Here, we generate a triangular grid from our 2D point data
    triang = tri.Triangulation(x[:],y[:])
    #---add filled contour plot
    cntr  = ax.tricontourf(triang, var[:], cmap=cmap, levels=levels, extend='both')
    color_bar = fig.colorbar(cntr, ax=ax)
    color_bar.ax.set_title(varPlotName[varName]) # label at top
    #color_bar.set_label(varName)    # label at side
    #---add line contour plot
    ax.tricontour(triang, var[:], levels=noContourLevels, linewidths=0.5, colors='black', linestyles="--")
  else:
    #---create a structured target grid
    minX = xVar[1] #np.min(x)
    minY = yVar[1] #np.min(y)
    maxX = xVar[2] #np.max(x)
    maxY = yVar[2] #np.max(y)
    noX = int( (maxX - minX) / dx )
    noY = int( (maxY - minY) / dx )
    X, Y = np.meshgrid(np.linspace(minX, maxX, noX), np.linspace(minY, maxY, noY))
    #---interpolate to target grid
    trgVar = griddata((x,y), var, (X,Y), method='linear')
    #---mask variables inside the geometry
    # Therefore, calculate the closest distance and remove if larger than dx
    xy = np.stack((x,y), axis=-1)
    XY = np.stack((X.reshape(-1), Y.reshape(-1)), axis=-1)
    dist, distId = cKDTree(xy).query(XY, 1)
    noTrgVar1 = trgVar.shape[1]
    for i,d in enumerate(dist):
      if d > 2.0 * dx:
        ii = i // noTrgVar1
        jj = i % noTrgVar1
        trgVar[ii, jj] = np.nan
    #---add filled contour plot
    cntr = ax.contourf(X, Y, trgVar, levels=levels, cmap=cmap, extend='both')
    color_bar = fig.colorbar(cntr, ax=ax)
    color_bar.ax.set_title(varPlotName[varName]) # label at top
    #---add line contour plot
    ax.contour(X, Y, trgVar, levels=levels, linewidths=0.5, colors='black', linestyles="--", extend='both')

  #plt.show() # if you just want to show these
  plt.savefig(outputFilePath, dpi='figure', format=outputFormat) #TODO: add flag to generate latex vector files
  plt.close()

class PlotHandler:
  def __init__(self, outputFilePath, figSize=(9, 7)):
    """
    figSize in cm
    """
    cm = 1/2.54  # centimeters in inches
    mpl.rcParams['figure.figsize']  = (figSize[0]*cm, figSize[1]*cm)
    print("Wirting {}".format(outputFilePath))
    self.outputFilePath = outputFilePath
    self.fig, self.axs = plt.subplots(nrows=1, ncols=1, squeeze=False)
    self.activeAx       = self.axs[0][0]
  def __del__(self):
    #self.save()
    pass
  def setLabels(self, xLabel, yLabel):
    """
      Set the labels of the active plot.
    """
    self.activeAx.set_xlabel(xLabel)
    self.activeAx.set_ylabel(yLabel)
  def setRanges(self, xRange=None, yRange=None):
    """
      Set the ranges of the active plot.
    """
    if xRange is not None: self.activeAx.set_xlim(xRange)
    if yRange is not None: self.activeAx.set_ylim(yRange)
  def plot(self, coords, data, label='', color=None, linestyle="solid", logX=False, logY=False):
    """
      Plot data over coords into the active plot.
      Possible to set logarithmic x-axis.
    """
    #TODO: check if color None would directly be valid instead of guarding
    if logX:
      self.activeAx.set_xscale('log', base=10)
    if logY:
      self.activeAx.set_yscale('log', base=10)
    if color is None:
      self.activeAx.plot(coords, data, label=label, color=color, linestyle=linestyle)
    else:
      self.activeAx.plot(coords, data, label=label, color=color, linestyle=linestyle)
  def legend(self, noCol=3):
    """
      Add a legend to the active plot.
    """
    self.activeAx.legend(bbox_to_anchor=(0.0,1.0), ncol=noCol)
  def save(self):
    self.fig.tight_layout()
    plt.savefig(self.outputFilePath, dpi='figure', format=outputFormat)

#---Main function
def main():
  #--make scalar plots of slices
  for institution in institutions:
    for config in configs:
      for inflowSpeed in inflowSpeeds:
        for sliceFileName in sliceFileNames:
          fileName      = getFileName(institution[0], config, inflowSpeed, sliceFileName[0])
          directory     = institution[1]
          inputFilePath = directory + fileName + ".h5"
          if not path.exists(inputFilePath):
            continue
          for varName in varNames:
            # TODO: check for valid path and create sub-directory figs if necessary
            outputFilePath  = directory + "figs/{}_{}.{}".format(fileName, varName, outputFormat)
            plotScalarField(inputFilePath, sliceFileName[1], sliceFileName[2], varName, outputFilePath)
  #--make plots over lines
  for config in configs:
    for inflowSpeed in inflowSpeeds:
      for lineFileName in lineFileNames:
        for varName in varNames:
          filePrefix = getFileName("ALL", config, inflowSpeed, lineFileName[0])
          outputFilePath  = "ALL/" + "figs/{}_{}.{}".format(filePrefix, varName, outputFormat)
          ph = PlotHandler(outputFilePath)
          coordName = lineFileName[1][0]
          varRange = varRanges[varName]
          for institution in institutions:
            fileName      = getFileName(institution[0], config, inflowSpeed, lineFileName[0])
            directory     = institution[1]
            inputFilePath = directory + fileName + ".csv"
            noHeaderRows = 0
            oldRow = None
            isDataAvailable = False
            if path.isfile(inputFilePath):
              for row in open(inputFilePath):
                if row[0] == "#":
                  noHeaderRows+=1
                  oldRow = row
                else:
                  colNames = oldRow.strip("# ").split()
                  break
              dataFile = pd.read_csv(inputFilePath, header=0, sep="\t", comment="#")
              dataFile.columns = colNames
              if coordName in dataFile and varName in dataFile:
                isDataAvailable = True
                coord = dataFile[coordName]
                var   = dataFile[varName]
                varRange = [min(varRange[0], np.min(var[:])), max(varRange[1], np.max(var[:]))]
                ph.plot(coord, var, label=institution[0])
            if not isDataAvailable:
              ph.plot([], [], label=institution[0])
          ph.setLabels(varPlotName[coordName], varPlotName[varName])
          ph.setRanges(lineFileName[1][1:3], varRange)
          ph.legend(2)
          ph.save()
  #--make SPL plots
  for config in configs:
    for inflowSpeed in inflowSpeeds:
      for splFileName in splFileNames:
        filePrefix = getFileName("ALL", config, inflowSpeed, splFileName[0])
        outputFilePath  = "ALL/" + "figs/{}.{}".format(filePrefix, outputFormat)
        ph = PlotHandler(outputFilePath)
        varName = splFileName[2]
        varRange = varRanges[varName]
        coordName = splFileName[1]
        coordRange = varRanges[coordName]
        for institution in institutions:
          fileName      = getFileName(institution[0], config, inflowSpeed, splFileName[0])
          directory     = institution[1]
          inputFilePath = directory + fileName + ".csv"
          noHeaderRows = 0
          oldRow = None
          isDataAvailable = False
          if path.isfile(inputFilePath):
            for row in open(inputFilePath):
              if row[0] == "#":
                noHeaderRows+=1
                oldRow = row
              else:
                colNames = oldRow.strip("# ").split()
                break
            dataFile = pd.read_csv(inputFilePath, header=0, sep="\t", comment="#")
            dataFile.columns = colNames
            if coordName in dataFile and varName in dataFile:
              isDataAvailable = True
              coord = dataFile[coordName]
              #coordRange = [min(coordRange[0], np.min(coord[:])), max(coordRange[1], np.max(coord[:]))]
              var   = dataFile[varName]
              #varRange = [min(varRange[0], np.min(var[:])), max(varRange[1], np.max(var[:]))]
              ph.plot(coord, var, label=institution[0], logX=True)
          if not isDataAvailable:
            ph.plot([], [], label=institution[0])
        ph.setLabels(varPlotName[coordName], varPlotName[varName])
        ph.setRanges(coordRange, varRange)
        ph.legend(2)
        ph.save()
  #--make plots of cp over curve
  for config in configs:
    for inflowSpeed in inflowSpeeds:
      for cpFileName in cpFileNames:
        varName = cpFileName[2]
        filePrefix = getFileName("ALL", config, inflowSpeed, cpFileName[0])
        outputFilePath  = "ALL/" + "figs/{}_{}.{}".format(filePrefix, varName, outputFormat)
        ph = PlotHandler(outputFilePath)
        coordName = cpFileName[1][0]
        varRange = varRanges[varName]
        for institution in institutions:
          fileName      = getFileName(institution[0], config, inflowSpeed, cpFileName[0])
          directory     = institution[1]
          inputFilePath = directory + fileName + ".csv"
          noHeaderRows = 0
          oldRow = None
          isDataAvailable = False
          if path.isfile(inputFilePath):
            for row in open(inputFilePath):
              if row[0] == "#":
                noHeaderRows+=1
                oldRow = row
              else:
                colNames = oldRow.strip("# ").split()
                break
            dataFile = pd.read_csv(inputFilePath, header=0, sep="\t", comment="#")
            dataFile.columns = colNames
            if coordName in dataFile and varName in dataFile:
              isDataAvailable = True
              coord = dataFile[coordName]
              var   = dataFile[varName]
              varRange = [min(varRange[0], np.min(var[:])), max(varRange[1], np.max(var[:]))]
              ph.plot(coord, var, label=institution[0])
          if not isDataAvailable:
            ph.plot([], [], label=institution[0])
          ph.setLabels(varPlotName[coordName], varPlotName[varName])
          ph.setRanges(cpFileName[1][1:3], varRange)
          ph.legend(2)
          ph.save()

#---
#---Python stuff
#---
if __name__ == '__main__':
  main()
