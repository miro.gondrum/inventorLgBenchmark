Objectives
================================================================================
- Cross-compare numerical methods and experimental measurements

Definitions
================================================================================
- U = 35 m/s
- D = 0.15 m
- BlBkTl    : baseline + brakes + torque link
- BlBkTlF01 : BlBkTl + fairing (Solid)
- BlBkTlF02 : BlBkTl + fairing (Diamond dc=2.5mm)
- BlBkTlF03 : BlBkTl + fairing (Diamond dc=4.5mm)
- BlBkTlF04 : BlBkTl + fairing (Diamond dc=6.4mm)
- BlBkTlF05 : BlBkTl + fairing (Metal Wool)
- BlBkTlF06 : BlBkTl + fairing (Wire Mesh ONERA)
- BlBkTlF07 : BlBkTl + fairing (Wire Mesh DLR)
- BlBkTlF08 : BlBkTl + fairing (Straight Perf D2 T3 e1)
- BlBkTlF09 : BlBkTl + fairing (Straight Perf. D2 T3 e2)
- BlBkTlF10 : BlBkTl + fairing (Straight Perf D4 T6 e1)
- BlBkTlF11 : BlBkTl + fairing (Straight Perf D5 T6 e1)
- BlBkTlF14 : BlBkTl + fairing (Or. Perf. AL D2 T3.2 Phi30 e2)
- BlBkTlF15 : BlBkTl + fairing (Or. Perf. AL D2 T3.2 Phi20 e2)
- BlBkTlF16 : BlBkTl + fairing (Or. Perf. AL D2 T3.2 Phi20 e1)
- BlBkTlF17 : BlBkTl + fairing (Oriented Perf. Zig Zag)
- BlBkTlF18 : BlBkTl + fairing (Oriented Perf. Left Right)
- BlBkTlF19 : BlBkTl + fairing (Or. Perf. Up Down-Left Right)
- BlBkTlF20 : BlBkTl + fairing (Helical Perf.)

## Coordinate system
(as proposed by TUD)
- Origin    : on the symmetry plane at the centerline of LG's axle
- x-axis    : direction of inflow
- y-axis    : from axle in direction to the wall the LG is attached to

## Dimensions
- Length    : Normalized by the wheel diameter (D)
- Velocities: Normalized by the inflow velocity (U)

## File naming
  `Inventor_benchmark_Config_U35_sampling_Institution.format`
  , e.g.,
  `Inventor_benchmark_BlBkTl_U35_planeYp0000_RWTH.h5`

Flow field
================================================================================
## Relevant quantities
- x,y,z           : probing positions
- uavg,vavg,wavg  : average of velocity components
- urms,vrms,wrms  : RMS of fluctuating velocity components

## Sampling planes
All planes are specified in the format (xmin,ymin,zmin | xmax,ymax,zmax) being
normal to one of the Cartesian directions.

> The planes from the experiment are chosen, but increased in its dimensions as
> it might be interesting to investigate the flow around the wheels and the axle.
> Original sizes from experiment included here just for reference
>  - planeXp1700 : (1.1333, -0.4500, -0.6000 | 1.1333, 0.5000, 0.6000) D at x=170.0mm
>  - planeYp0000 : (0.3500,  0.0000, -0.6000 | 2.0000, 0.0000, 0.6000) D at y=000.0mm

- planeXp1700 : ( 1.1333, -0.4500, -0.7000 |  1.1333, 0.5000, 0.7000) D

- planeYp0000 : (-0.5000,  0.0000, -0.7000 |  2.0000, 0.0000, 0.7000) D
- planeYp0500 : (-0.5000,  0.3333, -0.7000 |  2.0000, 0.3333, 0.7000) D
- planeYp1200 : (-0.5000,  0.8000, -0.7000 |  2.0000, 0.8000, 0.7000) D

- planeZp0000 : (-0.5000, -0.4500,  0.0000 |  2.0000, 0.8000, 0.0000) D

Following planes are used to compare the influence of the fairing.
First one is placed upstream (the other downstream) of the fairing.
- planeXn0675 : (-0.4500, -0.6000, -0.1500 | -0.4500, 1.2500, 0.1500) D
- planeXn0500 : (-0.3333, -0.6000, -0.1500 | -0.3333, 1.2500, 0.1500) D
- planeXn0300 : (-0.2000, -0.6000, -0.1500 | -0.2000, 1.2500, 0.1500) D

## Sampling lines
(redundant data extracted from sampling planes)
All lines are specified in the format (xmin,ymin,zmin | xmax,ymax,zmax)
- lineYp0000Zp0000  : (0.5000,  0.0000,  0.0000 | 2.0000, 0.0000, 0.0000) D
- lineXp1700Yp0000  : (1.1333,  0.0000, -0.7000 | 1.1333, 0.0000, 0.7000) D
- lineXp1700Zp0000  : (1.1333, -0.4500,  0.0000 | 1.1333, 0.5000, 0.0000) D
- TODO: Alexandros asked for furhter lines on plane z={50mm,120mm}

## Data format
All quantities are named as given under 'Relevant quantities'. Each quantity is
a 1D array/column with the length of number of probing points. Per plane/line
there is only one file containing all the relevant quantities.
- planes    : HDF5 file format (.h5, .mat)
- lines     : space or tabulator separated ASCI file (.csv)

Acoustic field
================================================================================
## Relevant quantities
Following TUD:
  "SPL_13  => Sound Pressure Level (in dB, ref 20 microPascal) obtained by
  integrating source maps within the selected integration region in 1/3 octave
  bands"
- f         : frequency in Hz
- SPL       : sound pressure level 1/3 octave band in dB, ref 20 microPascal

## Observer points
All microphones are specified in the format (x,y,z) D
- microLATH : ( 1.4667, -0.0000,  9.0667 ) D
- microFLYV : ( 1.4667, -7.0000, -0.1333 ) D

## Data format
- space or tabulator separated ASCI file (.csv)
Data arrays are stored in columns starting with the frequency. Each observer
point is then added in the order given by "Observer points".
