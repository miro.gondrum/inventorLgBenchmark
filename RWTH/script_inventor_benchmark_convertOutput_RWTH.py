#!/usr/bin/env python3

#-------------------------------------------------------------------------------
#---Description-----------------------------------------------------------------
#-------------------------------------------------------------------------------
# This script provides a layout for converting output from different partners of
# the INVENTOR project into a unified format facilitating comparison.
#
#---
#---Usage / Getting started
#---
# 0)  You need any python 3.x version and the packages listed in the next section
# 1)  Adjust the section 'Input' with your data
# 2)  In the section script classes and methods for writing the output in the
#     desired format are provided as well as a layout to write all the plane and
#     line files specified.
#     Here, reading your data is missing. Therefore, you need to provide your
#     data in numpy arrays which is currently done by a dummy function called
#     'readDummyFile(..)'.
# 3)  Perform the script and provide the generated data.
#
#---
#---Requirements
#---
# h5py : https://pypi.org/project/h5py
#   pip install h5py
# numpy : https://numpy.org/install/
#   pip install numpy
# scipy
#   pip install scipy
# pandas
#   pip install pandas
# netcdf4python
#   pip install netcdf4python

#-------------------------------------------------------------------------------
#---Input-----------------------------------------------------------------------
#-------------------------------------------------------------------------------
creator         = "Miro Gondrum"
institution     = "RWTH"
numericalMethod = "LBM / FWH (solver framework m-AIA)"
configs         = [
    "BlBkTl",
    "BlBkTlF01",
    "BlBkTlF02",
    "BlBkTlF04",
    ]
inflowSpeeds    = ["U35"]     # in [m/s] of experiment
creationDate    = None      # in format "YYYY-DD-MM" or None (set today)

outputDirectory = "./" #"./{}/".format(institution)
inputDirPrefixCfd = "./data/cfdPost/"
inputDirPrefixCaa = "./data/caa/"

createCFDData = True
createCAAData = True
createCPData  = True

#-------------------------------------------------------------------------------
#---Script----------------------------------------------------------------------
#-------------------------------------------------------------------------------
from os import path
import h5py
from netCDF4 import Dataset
from datetime import date
import numpy as np
from scipy.interpolate import griddata
from scipy import signal
import pandas as pd

if creationDate == None:
  creationDate = date.today()

def getFileName(config, inflowSpeed, name):
  return outputDirectory + "Inventor_benchmark_{}_{}_{}_{}".format(config,inflowSpeed,name,institution)

#---
#---Some wrapper classes for easier handling
#---
class h5FileHandler:
  """
  Wrapper class to facilitate the creation of a HDF5 file with correct
  formatting and meta information of the provided data sets.
  """
  def __init__(self, config, inflowSpeed, name):
    """
    """
    self.fileName = getFileName(config, inflowSpeed, name) + ".h5"
    self.file = h5py.File(self.fileName, 'w')
    # add some meta information
    self.file.attrs["Creator"]            = creator
    self.file.attrs["Institution"]        = institution
    self.file.attrs["Numerical methods"]  = numericalMethod
    self.file.attrs["Configuration"]      = config
    self.file.attrs["InflowSpeed"]        = inflowSpeed
    self.file.attrs["Date of creation"]   = "{}".format(creationDate)

  def writeFile(self):
    """
    """
    self.file.close()
    del self

  #def createDimension(self, name, array):
  #  """
  #  Create a variable that describe a dimension of another data set.
  #  """
  #  dataset = self.file.create_dataset(name, data=array, dtype='f8')
  #  dataset.make_scale(name)

  def createVariable(self, name, array, dimension=None):
    """
    Create a variable in the data file.
    """
    dataset = self.file.create_dataset(name, data=array, dtype='f8')
    #for i,d in enumerate(dimension):
    #  dataset.dims[i].attach_scale(self.file[d])

  def createVariables(self, names, arrays):
    for i,name in enumerate(names):
      self.createVariable(name, arrays[i])

class csvFileHandler:
  """
  Wrapper class to facilitate the creation of a csv file with correct formatting
  and header information.
  """
  def __init__(self, config, inflowSpeed, name):
    self.fileName = getFileName(config, inflowSpeed, name) + ".csv"
    self.config = config
    self.inflowSpeed = inflowSpeed
    self.name = name
    # Data containers
    self.variableNames = []
    self.variableDatas = []
    self.staticVariableDatas = []
  
  def createStaticDimension(self, name, var):
    self.staticVariableDatas.append([name, var])

  def createStaticDimensions(self, names, varis):
    for i,name in enumerate(names):
      self.createStaticDimension(name, varis[i])

  def createVariable(self, name, array):
    self.variableNames.append(name)
    self.variableDatas.append(array)

  def createVariables(self, names, arrays):
    for i,name in enumerate(names):
      self.createVariable(name, arrays[i])

  def writeFile(self):
    headerTxt = ""
    # Add meta information
    headerTxt += "Creator          : " + creator + "\n"
    headerTxt += "Institution      : " + institution + "\n"
    headerTxt += "Numerical methods: " + numericalMethod + "\n"
    headerTxt += "Configuration    : " + self.config + "\n"
    headerTxt += "InflowSpeed      : " + self.inflowSpeed + "\n"
    headerTxt += "Date of creation : {}".format(creationDate) + "\n"
    headerTxt += "\n"
    # Add static variables
    headerTxt += "Constant variables\n"
    for varData in self.staticVariableDatas:
      headerTxt += "{} = {}\n".format(varData[0], varData[1])
    headerTxt += "\n"
    # Add variable names
    for varName in self.variableNames:
      headerTxt += varName + "\t"
    # Write data
    outputData = np.array(self.variableDatas).T
    np.savetxt(self.fileName, outputData, fmt='%.18e', delimiter='\t', newline='\n', header=headerTxt, comments='# ')

class h5SampleOnLine:
  """
  Class to extract data on a line from surface data stored in a h5 file.
  """
  def __init__(self, config, inflowSpeed, name):
    self.fileName = getFileName(config, inflowSpeed, name) + ".h5"
    self.file = h5py.File(self.fileName, 'r')
  def close(self):
    self.file.close()
    self.file = None
  def setProbeLine(self, dirName0, dirName1, dirValue1):
    self.dir0 = self.file[dirName0[0]][:]
    self.dirValues = np.sort( np.unique(self.dir0) )
    self.dirValue1 = dirValue1
    self.dir1 = self.file[dirName1[0]][:]
    return self.dirValues
  def interpolateValue(self, varName):
    interpolMethod = 'nearest'
    #interpolMethod = 'linear'
    #interpolMethod = 'cubic'
    gridPoints = (self.dir0, self.dir1)
    trgPoints = (self.dirValues,
        np.full_like(self.dirValues,self.dirValue1))
    var = self.file[varName]
    interpolVars = griddata(gridPoints, var, trgPoints, method=interpolMethod)
    return interpolVars

class FileReaderNetcdf():
  def __init__(self, filePath, mode='r'):
    self.filePath = filePath
    self.mode     = mode
    self.file     = None
    self._open()
  def __del__(self):
    self._close()
  def _open(self):
    self.file = Dataset(self.filePath, self.mode)
  def _close(self):
    self.file.close()
  def __getitem__(self, varName):
    return self.file.variables[varName]

def convertUnit(inputArray, unitType):
  rho = 1.224 # kg/m^3
  cs  = 343.0 # m/s
  lengthConversion = 0.15 # [m]
  timeConversionFactor = lengthConversion / cs
  pressureConversionFactor = rho * cs**2
  F1BpRef = 1.0 / ( (20.0*10.0**-6) / pressureConversionFactor )# non-dim 20 Micropascal
  if unitType == "frequency":
    return inputArray * ( cs / lengthConversion / (2.0*np.pi) )
  elif unitType == "pressure":
    return inputArray * ( rho * cs**2 )
  elif unitType == "time":
    return inputArray * ( timeConversionFactor )

def calcSplThirdOctaveBand(time, pressure):
  """
  time : in s
  pressure  : in Pa, given in time domain
  return : frequency in Hz and SPL refering to 20microPa
  """
  # 1. transform into frequency space
  noWindows = 8 #16 #8
  #window = 'hamming'
  window = 'hann'
  scaling = 'density'
  #scaling = 'spectrum'
  noSamples = len(time)
  maxWindowLength = noSamples // noWindows
  windowLength = 2 ** int(np.log2(maxWindowLength))
  fs        = 1.0 / (time[1] - time[0])
  f, pF = signal.welch(pressure[:], fs=fs, nperseg=windowLength, window=window, scaling=scaling)#, average='median')
  # 2. apply 1/3 octave band filter
  #[12.5, 16, 20, 25, 31.5, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000, 12500, 16000, 20000]
  f13 = f
  pF13 = pF * 10.0 # 10 because delta_f in exp is 10 Hz. Is this correct ???
  # TODO
  # 3. calculate SPL and return
  pref = 20.0e-6 # [Pa]
  F1Bpref = 1.0 / pref
  splF13 = 10 * np.log10(pF13 * F1Bpref**2)
  print("DBG")
  print(f)
  return f13, splF13

#---
#---User defined functions: TODO remove/change/add to your needs
#---
def readDummyFile(fileName, varName):
  """
  Do whatever you need to get it in the right format (scaling, shifting, ..)
  The wrapper classes defined above demand for numpy array.
  So it might be useful to return with a helper function a numpy array
  read/extracted from your input data file.
  your 
  """
  # Here only a dummy array that is returned, for testing purpose
  return np.arange(0.0, 10.0, 1.0)

class csvFileReaderRwth():
  def __init__(self, config, inflowSpeed, nameTag):
    fileName = "Inventor_benchmark_{}_{}_{}_{}.csv".format(config, inflowSpeed, nameTag, institution)
    filePath = inputDirPrefixCfd + fileName
    self.data = pd.read_csv(filePath, sep=',')
  def getVariable(self, varName):
    var = self.data[varName]
    M = 0.10197689949
    cs = (1.0/3.0)**0.5
    scaleVelocity = 1.0 / (M * cs)
    if varName in ["um", "vm", "wm"]:
      var *= scaleVelocity
    if varName in ["u'", "v'", "w'"]:
      var = (abs(var))**0.5*scaleVelocity
    return var

#---
#---Main function: Here you have to add routines for reading your output as input
#---
def main():
  for config in configs:
    for inflowSpeed in inflowSpeeds:
      if createCFDData:
        #---Sampling planes
        planeXp1700 = h5FileHandler(config, inflowSpeed, "planeXp1700")
        # <<
        fileReader = csvFileReaderRwth(config, inflowSpeed, "planeXp1700")
        y = fileReader.getVariable("Coords_1")
        z = fileReader.getVariable("Coords_2")
        x = np.full_like(y, 170.0/150.0)
        uavg = fileReader.getVariable("um")
        vavg = fileReader.getVariable("wm")
        wavg = -fileReader.getVariable("vm")
        urms = fileReader.getVariable("u'")
        vrms = fileReader.getVariable("w'")
        wrms = fileReader.getVariable("v'")
        # >>
        planeXp1700.createVariables(["x","y","z"], [x,y,z])
        planeXp1700.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        planeXp1700.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        planeXp1700.writeFile()

        planeYp0000 = h5FileHandler(config, inflowSpeed, "planeYp0000")
        # <<
        fileReader = csvFileReaderRwth(config, inflowSpeed, "planeYp0000")
        x = fileReader.getVariable("Coords_0")
        z = fileReader.getVariable("Coords_2")
        y = np.full_like(x, 0.0/150.0)
        uavg = fileReader.getVariable("um")
        vavg = fileReader.getVariable("wm")
        wavg = -fileReader.getVariable("vm")
        urms = fileReader.getVariable("u'")
        vrms = fileReader.getVariable("w'")
        wrms = fileReader.getVariable("v'")
        # >>
        planeYp0000.createVariables(["x","y","z"], [x,y,z])
        planeYp0000.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        planeYp0000.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        planeYp0000.writeFile()

        planeYp0500 = h5FileHandler(config, inflowSpeed, "planeYp0500")
        # <<
        fileReader = csvFileReaderRwth(config, inflowSpeed, "planeYp0500")
        x = fileReader.getVariable("Coords_0")
        z = fileReader.getVariable("Coords_2")
        y = np.full_like(x, 50.0/150.0)
        uavg = fileReader.getVariable("um")
        vavg = fileReader.getVariable("wm")
        wavg = -fileReader.getVariable("vm")
        urms = fileReader.getVariable("u'")
        vrms = fileReader.getVariable("w'")
        wrms = fileReader.getVariable("v'")
        # >>
        planeYp0500.createVariables(["x","y","z"], [x,y,z])
        planeYp0500.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        planeYp0500.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        planeYp0500.writeFile()

        planeYp1200 = h5FileHandler(config, inflowSpeed, "planeYp1200")
        # <<
        fileReader = csvFileReaderRwth(config, inflowSpeed, "planeYp1200")
        x = fileReader.getVariable("Coords_0")
        z = fileReader.getVariable("Coords_2")
        y = np.full_like(x, 120.0/150.0)
        uavg = fileReader.getVariable("um")
        vavg = fileReader.getVariable("wm")
        wavg = -fileReader.getVariable("vm")
        urms = fileReader.getVariable("u'")
        vrms = fileReader.getVariable("w'")
        wrms = fileReader.getVariable("v'")
        # example data >>
        planeYp1200.createVariables(["x","y","z"], [x,y,z])
        planeYp1200.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        planeYp1200.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        planeYp1200.writeFile()

        # planeZp0000 = h5FileHandler(config, inflowSpeed, "planeZp0000")
        # # <<
        # fileReader = csvFileReaderRwth(config, inflowSpeed, "planeZp0000")
        # x = fileReader.getVariable("Coords_0")
        # y = fileReader.getVariable("Coords_1")
        # z = np.full_like(x, 0.0/150.0)
        # uavg = fileReader.getVariable("um")
        # vavg = fileReader.getVariable("wm")
        # wavg = -fileReader.getVariable("vm")
        # urms = fileReader.getVariable("u'")
        # vrms = fileReader.getVariable("w'")
        # wrms = fileReader.getVariable("v'")
        # # example data >>
        # planeZp0000.createVariables(["x","y","z"], [x,y,z])
        # planeZp0000.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        # planeZp0000.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        # planeZp0000.writeFile()

        planeXn0675 = h5FileHandler(config, inflowSpeed, "planeXn0675")
        # <<
        fileReader = csvFileReaderRwth(config, inflowSpeed, "planeXn0675")
        y = fileReader.getVariable("Coords_1")
        z = fileReader.getVariable("Coords_2")
        x = np.full_like(y, -67.5/150.0)
        uavg = fileReader.getVariable("um")
        vavg = fileReader.getVariable("wm")
        wavg = -fileReader.getVariable("vm")
        urms = fileReader.getVariable("u'")
        vrms = fileReader.getVariable("w'")
        wrms = fileReader.getVariable("v'")
        # >>
        planeXn0675.createVariables(["x","y","z"], [x,y,z])
        planeXn0675.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        planeXn0675.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        planeXn0675.writeFile()

        planeXn0500 = h5FileHandler(config, inflowSpeed, "planeXn0500")
        # <<
        fileReader = csvFileReaderRwth(config, inflowSpeed, "planeXn0500")
        y = fileReader.getVariable("Coords_1")
        z = fileReader.getVariable("Coords_2")
        x = np.full_like(y, -50.0/150.0)
        uavg = fileReader.getVariable("um")
        vavg = fileReader.getVariable("wm")
        wavg = -fileReader.getVariable("vm")
        urms = fileReader.getVariable("u'")
        vrms = fileReader.getVariable("w'")
        wrms = fileReader.getVariable("v'")
        # >>
        planeXn0500.createVariables(["x","y","z"], [x,y,z])
        planeXn0500.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        planeXn0500.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        planeXn0500.writeFile()

        planeXn0300 = h5FileHandler(config, inflowSpeed, "planeXn0300")
        # <<
        fileReader = csvFileReaderRwth(config, inflowSpeed, "planeXn0300")
        y = fileReader.getVariable("Coords_1")
        z = fileReader.getVariable("Coords_2")
        x = np.full_like(y, -30.0/150.0)
        uavg = fileReader.getVariable("um")
        vavg = fileReader.getVariable("wm")
        wavg = -fileReader.getVariable("vm")
        urms = fileReader.getVariable("u'")
        vrms = fileReader.getVariable("w'")
        wrms = fileReader.getVariable("v'")
        # >>
        planeXn0300.createVariables(["x","y","z"], [x,y,z])
        planeXn0300.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        planeXn0300.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        planeXn0300.writeFile()

        #---Sampling lines
        lineYp0000Zp0000 = csvFileHandler(config, inflowSpeed, "lineYp0000Zp0000")
        # <<
        planeYp0000 = h5SampleOnLine(config, inflowSpeed, "planeYp0000")
        y = 0.0/150.0
        z = 0.0/150.0
        x = planeYp0000.setProbeLine("x","z",z)
        uavg = planeYp0000.interpolateValue("uavg")
        vavg = planeYp0000.interpolateValue("vavg")
        wavg = planeYp0000.interpolateValue("wavg")
        urms = planeYp0000.interpolateValue("urms")
        vrms = planeYp0000.interpolateValue("vrms")
        wrms = planeYp0000.interpolateValue("wrms")
        # >>
        lineYp0000Zp0000.createStaticDimensions(["y","z"], [y,z])
        lineYp0000Zp0000.createVariable("x", x)
        lineYp0000Zp0000.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        lineYp0000Zp0000.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        lineYp0000Zp0000.writeFile()
        
        lineXp1700Yp0000 = csvFileHandler(config, inflowSpeed, "lineXp1700Yp0000")
        # <<
        x = 170.0/150.0
        y =   0.0/150.0
        z = planeYp0000.setProbeLine("z","x",x)
        uavg = planeYp0000.interpolateValue("uavg")
        vavg = planeYp0000.interpolateValue("vavg")
        wavg = planeYp0000.interpolateValue("wavg")
        urms = planeYp0000.interpolateValue("urms")
        vrms = planeYp0000.interpolateValue("vrms")
        wrms = planeYp0000.interpolateValue("wrms")
        # example data >>
        lineXp1700Yp0000.createStaticDimensions(["x","y"], [x,y])
        lineXp1700Yp0000.createVariable("z", z)
        lineXp1700Yp0000.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        lineXp1700Yp0000.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        lineXp1700Yp0000.writeFile()
        
        lineXp1700Zp0000 = csvFileHandler(config, inflowSpeed, "lineXp1700Zp0000")
        # << 
        planeXp1700 = h5SampleOnLine(config, inflowSpeed, "planeXp1700")
        x = 170.0/150.0
        z =   0.0/150.0
        y = planeXp1700.setProbeLine("y","z",z)
        uavg = planeXp1700.interpolateValue("uavg")
        vavg = planeXp1700.interpolateValue("vavg")
        wavg = planeXp1700.interpolateValue("wavg")
        urms = planeXp1700.interpolateValue("urms")
        vrms = planeXp1700.interpolateValue("vrms")
        wrms = planeXp1700.interpolateValue("wrms")
        # >>
        lineXp1700Zp0000.createStaticDimensions(["x","z"], [x,z])
        lineXp1700Zp0000.createVariable("y", y)
        lineXp1700Zp0000.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        lineXp1700Zp0000.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        lineXp1700Zp0000.writeFile()

      if createCAAData:
        #---Acoustic microphone data
        micros = [
            ["microCmFlyv", 1, [1.4667, 0.0000,  9.0667]],
            ["microCmLath", 0, [1.4667, -7.0000,  -0.1333]],
            ]
        for micro in micros:
          # << example data TODO: replace following by reading your data
          #fileName = "SPL.Netcdf" #TODO: update with correct naming including config
          fileName = "observerData_{}.Netcdf".format(config)
          filePath = inputDirPrefixCaa + fileName
          if not path.exists(filePath):
            continue
          fileReader = FileReaderNetcdf(filePath)
          observerId = micro[1]
          #freq = fileReader["frequency"]
          #spl = fileReader["SPL"][observerId]
          time = fileReader["time"]
          pressure = fileReader["pressure_time"][observerId]
          time = convertUnit(time[:], "time")
          pressure = convertUnit(pressure[:], "pressure")
          freq, spl = calcSplThirdOctaveBand(time, pressure)
          # example data >>
          microOut = csvFileHandler(config, inflowSpeed, micro[0])
          microOut.createStaticDimensions(["x","y","z"], micro[2] )
          #microOut.createVariables(["freq._13[Hz]","SPL_13[20microPa]"], [freq,spl])
          microOut.createVariables(["freq._BF[Hz]","SPL_BF[20microPa]"], [freq,spl])
          microOut.writeFile()

      if createCPData:
        cpLeftWheel = csvFileHandler(config, inflowSpeed, "cpLeftWheel")
        fileReader = csvFileReaderRwth(config, inflowSpeed, "cpLeftWheel")
        x = fileReader.getVariable("Points_0")
        y = fileReader.getVariable("Points_2") + 2.33333
        cp = fileReader.getVariable("cp")
        theta = (np.pi - np.arctan2(y,x)) * 180.0/np.pi
        # << sorting
        idx = np.argsort(theta)
        x = np.array(x)[idx]
        y = np.array(y)[idx]
        theta = np.array(theta)[idx]
        cp = np.array(cp)[idx]
        # sorting >>
        #cpLeftWheel.createVariables(["x","y","theta"], [x,y,theta])
        cpLeftWheel.createVariables(["theta", "cp"], [theta, cp])
        cpLeftWheel.writeFile()
  
#---
#---Python stuff
#---
if __name__ == '__main__':
  main()
