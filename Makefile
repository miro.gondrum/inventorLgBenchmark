
updatePlots:
	@./plot_inventor_benchmark.py
	@cd pdfVersion ; pdflatex inventor_benchmark_crossPlot.tex ; cd -

updateTudExp:
	@cd TUD_exp ; ./script_inventor_benchmark_convertOutput_TUDexp.py ; cd -

updateRWTH:
	@cd RWTH ; ./script_inventor_benchmark_convertOutput_RWTH.py ; cd -

ALL: updateTudExp updateRWTH updatePlots
