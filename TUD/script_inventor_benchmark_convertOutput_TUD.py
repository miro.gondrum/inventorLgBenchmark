#!/usr/bin/env python3

#-------------------------------------------------------------------------------
#---Description-----------------------------------------------------------------
#-------------------------------------------------------------------------------
# This script provides a layout for converting output from different partners of
# the INVENTOR project into a unified format facilitating comparison.
#
#---
#---Usage / Getting started
#---
# 0)  You need any python 3.x version and the packages listed in the next section
# 1)  Adjust the section 'Input' with your data
# 2)  In the section script classes and methods for writing the output in the
#     desired format are provided as well as a layout to write all the plane and
#     line files specified.
#     Here, reading your data is missing. Therefore, you need to provide your
#     data in numpy arrays which is currently done by a dummy function called
#     'readDummyFile(..)'.
# 3)  Perform the script and provide the generated data.
#
#---
#---Requirements
#---
# h5py : https://pypi.org/project/h5py
#   pip install h5py
# numpy : https://numpy.org/install/
#   pip install numpy
# scipy
#   pip install scipy

#-------------------------------------------------------------------------------
#---Input-----------------------------------------------------------------------
#-------------------------------------------------------------------------------
creator         = ""
institution     = "TUD"
numericalMethod = "CFD/CAA simulation using PowerFlow"
configs         = [
    "BlBkTl",
    "BlBkTlF01",
    "BlBkTlF02",
    ]
inflowSpeeds    = ["U35"]     # in [m/s] of experiment
creationDate    = None      # in format "YYYY-DD-MM" or None (set today)

inputDirPrefix = "./data/"
outputDirectory = "./" #"./{}/".format(institution)

createCFDData = True
createCAAData = False #True
#createCPData = True

#-------------------------------------------------------------------------------
#---Script----------------------------------------------------------------------
#-------------------------------------------------------------------------------
import h5py
from datetime import date
import numpy as np
from scipy.interpolate import griddata
import scipy.io
import os.path
import sys

if creationDate == None:
  creationDate = date.today()

def getFileName(config, inflowSpeed, name):
  return outputDirectory + "Inventor_benchmark_{}_{}_{}_{}".format(config,inflowSpeed,name,institution)

#---
#---Some wrapper classes for easier handling
#---
class h5FileHandler:
  """
  Wrapper class to facilitate the creation of a HDF5 file with correct
  formatting and meta information of the provided data sets.
  """
  def __init__(self, config, inflowSpeed, name):
    """
    """
    self.fileName = getFileName(config, inflowSpeed, name) + ".h5"
    self.file = h5py.File(self.fileName, 'w')
    # add some meta information
    self.file.attrs["Creator"]            = creator
    self.file.attrs["Institution"]        = institution
    self.file.attrs["Numerical methods"]  = numericalMethod
    self.file.attrs["Configuration"]      = config
    self.file.attrs["InflowSpeed"]        = inflowSpeed
    self.file.attrs["Date of creation"]   = "{}".format(creationDate)
    print("Create {}".format(self.fileName))

  def writeFile(self):
    """
    """
    self.file.close()
    del self

  #def createDimension(self, name, array):
  #  """
  #  Create a variable that describe a dimension of another data set.
  #  """
  #  dataset = self.file.create_dataset(name, data=array, dtype='f8')
  #  dataset.make_scale(name)

  def createVariable(self, name, array, dimension=None):
    """
    Create a variable in the data file.
    """
    dataset = self.file.create_dataset(name, data=array, dtype='f8')
    #for i,d in enumerate(dimension):
    #  dataset.dims[i].attach_scale(self.file[d])

  def createVariables(self, names, arrays):
    for i,name in enumerate(names):
      self.createVariable(name, arrays[i])

class csvFileHandler:
  """
  Wrapper class to facilitate the creation of a csv file with correct formatting
  and header information.
  """
  def __init__(self, config, inflowSpeed, name):
    self.fileName = getFileName(config, inflowSpeed, name) + ".csv"
    self.config = config
    self.inflowSpeed = inflowSpeed
    self.name = name
    # Data containers
    self.variableNames = []
    self.variableDatas = []
    self.staticVariableDatas = []
    print("Create {}".format(self.fileName))
  
  def createStaticDimension(self, name, var):
    self.staticVariableDatas.append([name, var])

  def createStaticDimensions(self, names, varis):
    for i,name in enumerate(names):
      self.createStaticDimension(name, varis[i])

  def createVariable(self, name, array):
    self.variableNames.append(name)
    self.variableDatas.append(array)

  def createVariables(self, names, arrays):
    for i,name in enumerate(names):
      self.createVariable(name, arrays[i])

  def writeFile(self):
    headerTxt = ""
    # Add meta information
    headerTxt += "Creator          : " + creator + "\n"
    headerTxt += "Institution      : " + institution + "\n"
    headerTxt += "Numerical methods: " + numericalMethod + "\n"
    headerTxt += "Configuration    : " + self.config + "\n"
    headerTxt += "InflowSpeed      : " + self.inflowSpeed + "\n"
    headerTxt += "Date of creation : {}".format(creationDate) + "\n"
    headerTxt += "\n"
    # Add static variables
    headerTxt += "Constant variables\n"
    for varData in self.staticVariableDatas:
      headerTxt += "{} = {}\n".format(varData[0], varData[1])
    headerTxt += "\n"
    # Add variable names
    for varName in self.variableNames:
      headerTxt += varName + "\t"
    # Write data
    outputData = np.array(self.variableDatas).T
    np.savetxt(self.fileName, outputData, fmt='%.18e', delimiter='\t', newline='\n', header=headerTxt, comments='# ')

class h5SampleOnLine:
  """
  Class to extract data on a line from surface data stored in a h5 file.
  """
  def __init__(self, config, inflowSpeed, name):
    self.fileName = getFileName(config, inflowSpeed, name) + ".h5"
    self.file = h5py.File(self.fileName, 'r')
  def close(self):
    self.file.close()
    self.file = None
  def setProbeLine(self, dirName0, dirName1, dirValue1):
    self.dir0 = self.file[dirName0[0]][:]
    self.dirValues = np.sort( np.unique(self.dir0) )
    self.dirValue1 = dirValue1
    self.dir1 = self.file[dirName1[0]][:]
    return self.dirValues
  def interpolateValue(self, varName):
    #interpolMethod = 'nearest'
    interpolMethod = 'linear'
    #interpolMethod = 'cubic'
    gridPoints = (self.dir0, self.dir1)
    trgPoints = (self.dirValues,
        np.full_like(self.dirValues,self.dirValue1))
    var = self.file[varName]
    interpolVars = griddata(gridPoints, var, trgPoints, method=interpolMethod)
    return interpolVars

#---
#---User defined functions
#---
def readDummyFile(fileName, varName):
  """
  Do whatever you need to get it in the right format (scaling, shifting, ..)
  The wrapper classes defined above demand for numpy array.
  So it might be useful to return with a helper function a numpy array
  read/extracted from your input data file.
  your 
  """
  # Here only a dummy array that is returned, for testing purpose
  return np.arange(0.0, 10.0, 1.0)

def getFilePathTUD(config, inflowSpeed, name):
  institution = "TUD"
  name = name[0].upper() + name[1:]
  fileName = "Inventor_benchmark_{}_{}_{}_{}.mat".format(config,inflowSpeed,name,institution)
  filePath = inputDirPrefix + fileName
  return filePath

def readMatFilesTUD(filePath, varName):
  file = scipy.io.loadmat(filePath)
  varNameMap = {
      "x"     : 0,
      "y"     : 1,
      "z"     : 2,
      "uavg"  : 3,
      "vavg"  : 4,
      "wavg"  : 5,
      "urms"  : 6,
      "vrms"  : 7,
      "wrms"  : 8,
      }
  config_="BlBkTl" # Used in all files
  var = file[config_].T[varNameMap[varName]]
  return var

#---
#---Main function: Here you have to add routines for reading your output as input
#---
def main():
  for config in configs:
    for inflowSpeed in inflowSpeeds:
      if createCFDData:
        #---Sampling planes
        name = "planeXp1700"
        filePath = getFilePathTUD(config, inflowSpeed, name)
        if os.path.exists(filePath):
          planeXp1700 = h5FileHandler(config, inflowSpeed, name)
          # <<
          y = readMatFilesTUD(filePath, "y")
          z = readMatFilesTUD(filePath, "z")
          x = np.full_like(y, 170.0/150.0)
          uavg = readMatFilesTUD(filePath, "uavg")
          vavg = readMatFilesTUD(filePath, "vavg")
          wavg = readMatFilesTUD(filePath, "wavg")
          urms = readMatFilesTUD(filePath, "urms")
          vrms = readMatFilesTUD(filePath, "vrms")
          wrms = readMatFilesTUD(filePath, "wrms")
          # >>
          planeXp1700.createVariables(["x","y","z"], [x,y,z])
          planeXp1700.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
          planeXp1700.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
          planeXp1700.writeFile()

        name = "planeYp0000"
        filePath = getFilePathTUD(config, inflowSpeed, name)
        if os.path.exists(filePath):
          planeYp0000 = h5FileHandler(config, inflowSpeed, name)
          # <<
          x = readMatFilesTUD(filePath, "x")
          z = readMatFilesTUD(filePath, "z")
          y = np.full_like(x, 0.0/150.0)
          uavg = readMatFilesTUD(filePath, "uavg")
          vavg = readMatFilesTUD(filePath, "vavg")
          wavg = readMatFilesTUD(filePath, "wavg")
          urms = readMatFilesTUD(filePath, "urms")
          vrms = readMatFilesTUD(filePath, "vrms")
          wrms = readMatFilesTUD(filePath, "wrms")
          # >>
          planeYp0000.createVariables(["x","y","z"], [x,y,z])
          planeYp0000.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
          planeYp0000.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
          planeYp0000.writeFile()

        name = "planeYp0500"
        filePath = getFilePathTUD(config, inflowSpeed, name)
        if os.path.exists(filePath):
          planeYp0500 = h5FileHandler(config, inflowSpeed, name)
          # <<
          x = readMatFilesTUD(filePath, "x")
          z = readMatFilesTUD(filePath, "z")
          y = np.full_like(x, 50.0/150.0)
          uavg = readMatFilesTUD(filePath, "uavg")
          vavg = readMatFilesTUD(filePath, "vavg")
          wavg = readMatFilesTUD(filePath, "wavg")
          urms = readMatFilesTUD(filePath, "urms")
          vrms = readMatFilesTUD(filePath, "vrms")
          wrms = readMatFilesTUD(filePath, "wrms")
          # >>
          planeYp0500.createVariables(["x","y","z"], [x,y,z])
          planeYp0500.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
          planeYp0500.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
          planeYp0500.writeFile()

        name = "planeYp1200"
        filePath = getFilePathTUD(config, inflowSpeed, name)
        if os.path.exists(filePath):
          planeYp1200 = h5FileHandler(config, inflowSpeed, name)
          # <<
          x = readMatFilesTUD(filePath, "x")
          z = readMatFilesTUD(filePath, "z")
          y = np.full_like(x, 120.0/150.0)
          uavg = readMatFilesTUD(filePath, "uavg")
          vavg = readMatFilesTUD(filePath, "vavg")
          wavg = readMatFilesTUD(filePath, "wavg")
          urms = readMatFilesTUD(filePath, "urms")
          vrms = readMatFilesTUD(filePath, "vrms")
          wrms = readMatFilesTUD(filePath, "wrms")
          # >>
          planeYp1200.createVariables(["x","y","z"], [x,y,z])
          planeYp1200.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
          planeYp1200.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
          planeYp1200.writeFile()

        name = "planeZp0000"
        filePath = getFilePathTUD(config, inflowSpeed, name)
        if os.path.exists(filePath):
          planeZp0000 = h5FileHandler(config, inflowSpeed, name)
          # <<
          x = readMatFilesTUD(filePath, "x")
          y = readMatFilesTUD(filePath, "y")
          z = np.full_like(x, 0.0/150.0)
          uavg = readMatFilesTUD(filePath, "uavg")
          vavg = readMatFilesTUD(filePath, "vavg")
          wavg = readMatFilesTUD(filePath, "wavg")
          urms = readMatFilesTUD(filePath, "urms")
          vrms = readMatFilesTUD(filePath, "vrms")
          wrms = readMatFilesTUD(filePath, "wrms")
          # >>
          planeZp0000.createVariables(["x","y","z"], [x,y,z])
          planeZp0000.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
          planeZp0000.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
          planeZp0000.writeFile()

        # TODO: only 6 instead of 9 entries in raw data file
        #name = "planeXn0675"
        #filePath = getFilePathTUD(config, inflowSpeed, name)
        #if os.path.exists(filePath):
        #  planeXn0675 = h5FileHandler(config, inflowSpeed, name)
        #  # << example data TODO: replace following by reading your data
        #  y = readMatFilesTUD(filePath, "y")
        #  z = readMatFilesTUD(filePath, "z")
        #  x = np.full_like(y, -67.5/150.0)
        #  uavg = readMatFilesTUD(filePath, "uavg")
        #  vavg = readMatFilesTUD(filePath, "vavg")
        #  wavg = readMatFilesTUD(filePath, "wavg")
        #  urms = readMatFilesTUD(filePath, "urms")
        #  vrms = readMatFilesTUD(filePath, "vrms")
        #  wrms = readMatFilesTUD(filePath, "wrms")
        #  # example data >>
        #  planeXn0675.createVariables(["x","y","z"], [x,y,z])
        #  planeXn0675.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        #  planeXn0675.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        #  planeXn0675.writeFile()

        # TODO: only 6 instead of 9 entries in raw data file
        #name = "planeXn0500"
        #filePath = getFilePathTUD(config, inflowSpeed, name)
        #if os.path.exists(filePath):
        #  planeXn0500 = h5FileHandler(config, inflowSpeed, name)
        #  # << example data TODO: replace following by reading your data
        #  y = readMatFilesTUD(filePath, "y")
        #  z = readMatFilesTUD(filePath, "z")
        #  x = np.full_like(y, -50.0/150.0)
        #  uavg = readMatFilesTUD(filePath, "uavg")
        #  vavg = readMatFilesTUD(filePath, "vavg")
        #  wavg = readMatFilesTUD(filePath, "wavg")
        #  urms = readMatFilesTUD(filePath, "urms")
        #  vrms = readMatFilesTUD(filePath, "vrms")
        #  wrms = readMatFilesTUD(filePath, "wrms")
        #  # example data >>
        #  planeXn0500.createVariables(["x","y","z"], [x,y,z])
        #  planeXn0500.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        #  planeXn0500.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        #  planeXn0500.writeFile()

        # TODO: only 6 instead of 9 entries in raw data file
        #name = "planeXn0300"
        #filePath = getFilePathTUD(config, inflowSpeed, name)
        #if os.path.exists(filePath):
        #  planeXn0300 = h5FileHandler(config, inflowSpeed, name)
        #  # << example data TODO: replace following by reading your data
        #  y = readMatFilesTUD(filePath, "y")
        #  z = readMatFilesTUD(filePath, "z")
        #  x = np.full_like(y, -30.0/150.0)
        #  uavg = readMatFilesTUD(filePath, "uavg")
        #  vavg = readMatFilesTUD(filePath, "vavg")
        #  wavg = readMatFilesTUD(filePath, "wavg")
        #  urms = readMatFilesTUD(filePath, "urms")
        #  vrms = readMatFilesTUD(filePath, "vrms")
        #  wrms = readMatFilesTUD(filePath, "wrms")
        #  # example data >>
        #  planeXn0300.createVariables(["x","y","z"], [x,y,z])
        #  planeXn0300.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        #  planeXn0300.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        #  planeXn0300.writeFile()

        #---Sampling lines
        lineYp0000Zp0000 = csvFileHandler(config, inflowSpeed, "lineYp0000Zp0000")
        # <<
        planeYp0000 = h5SampleOnLine(config, inflowSpeed, "planeYp0000")
        y = 0.0/150.0
        z = 0.0/150.0
        x = planeYp0000.setProbeLine("x","z",z)
        uavg = planeYp0000.interpolateValue("uavg")
        vavg = planeYp0000.interpolateValue("vavg")
        wavg = planeYp0000.interpolateValue("wavg")
        urms = planeYp0000.interpolateValue("urms")
        vrms = planeYp0000.interpolateValue("vrms")
        wrms = planeYp0000.interpolateValue("wrms")
        # >>
        lineYp0000Zp0000.createStaticDimensions(["y","z"], [y,z])
        lineYp0000Zp0000.createVariable("x", x)
        lineYp0000Zp0000.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        lineYp0000Zp0000.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        lineYp0000Zp0000.writeFile()
        
        lineXp1700Yp0000 = csvFileHandler(config, inflowSpeed, "lineXp1700Yp0000")
        # <<
        x = 170.0/150.0
        y =   0.0/150.0
        z = planeYp0000.setProbeLine("z","x",x)
        uavg = planeYp0000.interpolateValue("uavg")
        vavg = planeYp0000.interpolateValue("vavg")
        wavg = planeYp0000.interpolateValue("wavg")
        urms = planeYp0000.interpolateValue("urms")
        vrms = planeYp0000.interpolateValue("vrms")
        wrms = planeYp0000.interpolateValue("wrms")
        # example data >>
        lineXp1700Yp0000.createStaticDimensions(["x","y"], [x,y])
        lineXp1700Yp0000.createVariable("z", z)
        lineXp1700Yp0000.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        lineXp1700Yp0000.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        lineXp1700Yp0000.writeFile()
        
        lineXp1700Zp0000 = csvFileHandler(config, inflowSpeed, "lineXp1700Zp0000")
        # << 
        planeXp1700 = h5SampleOnLine(config, inflowSpeed, "planeXp1700")
        x = 170.0/150.0
        z =   0.0/150.0
        y = planeXp1700.setProbeLine("y","z",z)
        uavg = planeXp1700.interpolateValue("uavg")
        vavg = planeXp1700.interpolateValue("vavg")
        wavg = planeXp1700.interpolateValue("wavg")
        urms = planeXp1700.interpolateValue("urms")
        vrms = planeXp1700.interpolateValue("vrms")
        wrms = planeXp1700.interpolateValue("wrms")
        # >>
        lineXp1700Zp0000.createStaticDimensions(["x","z"], [x,z])
        lineXp1700Zp0000.createVariable("y", y)
        lineXp1700Zp0000.createVariables(["uavg","vavg","wavg"], [uavg,vavg,wavg])
        lineXp1700Zp0000.createVariables(["urms","vrms","wrms"], [urms,vrms,wrms])
        lineXp1700Zp0000.writeFile()

      #if createCAAData:
      #  #---Acoustic microphone data
      #  microCmFlyv = csvFileHandler(config, inflowSpeed, "microCmFlyv")
      #  # << 
      #  fileName = mapConfigFileNameAcoustic[config][0]
      #  #freqCM = readMatFilesTUD2(fileName, "f_CM")
      #  #splCM  = readMatFilesTUD2(fileName, "SPL_CM")
      #  freqBF = readMatFilesTUD2(fileName, "f_BF")
      #  splBF  = readMatFilesTUD2(fileName, "SPL_BF")
      #  #freq13 = readMatFilesTUD2(fileName, "f13")
      #  #spl13  = readMatFilesTUD2(fileName, "SPL13")
      #  #freqMM = readMatFilesTUD2(fileName, "f_MM")
      #  #splMM  = readMatFilesTUD2(fileName, "SPL_MM")
      #  # >>
      #  microCmFlyv.createStaticDimensions(["x","y","z"], [1.4667, 0.0000,  9.0667])
      #  #microCmFlyv.createVariables(["freq._CM[Hz]","SPL_CM[20microPa]"], [freqCM,splCM])
      #  microCmFlyv.createVariables(["freq._BF[Hz]","SPL_BF[20microPa]"], [freqBF,splBF])
      #  #microCmFlyv.createVariables(["freq._13[Hz]","SPL_13[20microPa]"], [freq13,spl13])
      #  #microCmFlyv.createVariables(["freq._MM[Hz]","SPL_MM[20microPa]"], [freqMM,splMM])
      #  microCmFlyv.writeFile()
      #  
      #  microCmLath = csvFileHandler(config, inflowSpeed, "microCmLath")
      #  # <<
      #  fileName = mapConfigFileNameAcoustic[config][1]
      #  #freqCM = readMatFilesTUD2(fileName, "f_CM")
      #  #splCM  = readMatFilesTUD2(fileName, "SPL_CM")
      #  freqBF = readMatFilesTUD2(fileName, "f_BF")
      #  splBF  = readMatFilesTUD2(fileName, "SPL_BF")
      #  #freq13 = readMatFilesTUD2(fileName, "f13")
      #  #spl13  = readMatFilesTUD2(fileName, "SPL13")
      #  #freqMM = readMatFilesTUD2(fileName, "f_MM")
      #  #splMM  = readMatFilesTUD2(fileName, "SPL_MM")
      #  # >>
      #  microCmLath.createStaticDimensions(["x","y","z"], [1.4667, -7.0000,  -0.1333])
      #  #microCmLath.createVariables(["freq._CM[Hz]","SPL_CM[20microPa]"], [freqCM,splCM])
      #  microCmLath.createVariables(["freq._BF[Hz]","SPL_BF[20microPa]"], [freqBF,splBF])
      #  #microCmLath.createVariables(["freq._13[Hz]","SPL_13[20microPa]"], [freq13,spl13])
      #  #microCmLath.createVariables(["freq._MM[Hz]","SPL_MM[20microPa]"], [freqMM,splMM])
      #  microCmLath.writeFile()

      ##---Pressure taps
      #if createCPData:
      #  import pandas as pd
      #  cpLeftWheel = csvFileHandler(config, inflowSpeed, "cpLeftWheel")
      #  filePath = "./data/Inventor_benchmark_{}_{}_cpLeftWheel_TUD_exp.csv".format(config, inflowSpeed)
      #  data = pd.read_csv(filePath, sep=',')
      #  print(data)
      #  theta = data["theta"]
      #  cp = data["cp"]
      #  cpLeftWheel.createVariables(["theta", "cp"], [theta, cp])
      #  cpLeftWheel.writeFile()
  
#---
#---Python stuff
#---
if __name__ == '__main__':
  main()
